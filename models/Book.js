const { Schema, model } = require("mongoose");

const schema = new Schema({
  name: String,
  genre: String,
  authorId: String,
});

module.exports = model("book", schema);
