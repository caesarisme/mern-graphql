import React from "react";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "@apollo/react-hooks";

// Components
import BookList from "./components/BookList";
import AddBook from "./components/AddBook";

// Apollo client
const client = new ApolloClient({
  uri: "/graphql",
});

function App() {
  return (
    <ApolloProvider client={client}>
      <div className="main">
        <h1>Ninja's Reading list</h1>
        <BookList />
        <hr />
        <AddBook />
      </div>
    </ApolloProvider>
  );
}

export default App;
