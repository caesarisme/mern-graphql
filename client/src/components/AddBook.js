import React, { Fragment, useState } from "react";
import { useQuery, useMutation } from "@apollo/react-hooks";

import { getAuthorsQuery, getBooksQuery, addBookMutation } from "../queries";

const AddBook = () => {
  const { loading, data: authorsData } = useQuery(getAuthorsQuery);
  const [form, setForm] = useState({ name: "", genre: "", authorId: "" });
  const [addBook] = useMutation(addBookMutation);

  const submitHandler = async (e) => {
    e.preventDefault();
    addBook({
      variables: {
        name: form.name,
        genre: form.genre,
        authorId: form.authorId,
      },
      refetchQueries: [{ query: getBooksQuery }],
    });
  };

  return (
    <form onSubmit={submitHandler}>
      <div className="field">
        <label>Book name:</label>
        <input
          type="text"
          onChange={(e) => setForm({ ...form, name: e.target.value })}
        />
      </div>

      <div className="field">
        <label>Genre:</label>
        <input
          type="text"
          onChange={(e) => setForm({ ...form, genre: e.target.value })}
        />
      </div>

      <div className="field">
        <label>Author:</label>
        <select
          onChange={(e) => setForm({ ...form, authorId: e.target.value })}
        >
          {loading ? (
            <option>Loading...</option>
          ) : (
            <Fragment>
              <option selected value={null}>
                Select author
              </option>
              {authorsData.authors.map((author) => (
                <option key={author.id} value={author.id}>
                  {author.name}
                </option>
              ))}
            </Fragment>
          )}
        </select>
      </div>

      <button type="submit">+</button>
    </form>
  );
};

export default AddBook;
