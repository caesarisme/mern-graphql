const express = require("express");
const mongoose = require("mongoose");
const graphqlHTTP = require("express-graphql");
const schema = require("./schema");

const app = express();

mongoose.connect("mongodb://localhost:27017/graphql-react", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

app.use(
  "/graphql",
  graphqlHTTP({
    schema,
    graphiql: true,
  }),
);

const PORT = 5000;
app.listen(PORT, () => console.log(`Listening port ${PORT}...`));
